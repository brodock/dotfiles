bundle config build.thin --with-cflags="-Wno-error=implicit-function-declaration"
bundle config build.grpc --with-cflags="-Wno-error=implicit-function-declaration"
bundle config set --global build.re2 --with-re2-dir=/opt/homebrew
