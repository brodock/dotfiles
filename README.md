# Dotfiles

## Prerequirements

Install homebrew to get initial dependencies and applications installed:

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Run Brew Bundle:

```bash
brew bundle
```

Install Oh My Fish:

```bash
curl -L https://get.oh-my.fish | fish
omf install bobthefish git gem rvm bundler z
```

Install RVM:

```bash
gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
echo progress-bar >> ~/.curlrc
\curl -sSL https://get.rvm.io | bash
```

Change to Fish as default shell:

```bash
chsh -s /usr/local/bin/fish
```

Monaco patched nerd font:
* https://github.com/Karmenzind/monaco-nerd-fonts
