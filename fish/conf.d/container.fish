# Docker
function docker-clean -d "Remove exited docker instances"
  docker ps --all -q -f status=exited | xargs docker rm
end

function docker-image-clean -d "Remove untagged images"
  docker images -f dangling=true -q | xargs docker rmi
end

function docker-stop-all -d "Stop all containers"
  docker ps -a -q | xargs docker stop
end

function docker-rm-all -d "Remove all containers"
  docker ps -a -q | xargs docker rm
end

function docker-run -d "Run docker container"
  docker run --rm -ti $argv
end

# Nerdctl
function nerd-clean -d "Remove exited container instances"
  nerdctl ps --all -q -f status=exited | xargs nerdctl rm
end

function nerd-image-clean -d "Remove untagged images"
  nerdctl images -f dangling=true -q | xargs nerdctl rmi
end

function nerd-stop-all -d "Stop all containers"
  nerdctl ps -a -q | xargs nerdctl stop
end

function nerd-rm-all -d "Remove all containers"
  nerdctl ps -a -q | xargs nerdctl rm
end

function docker-run -d "Run docker container"
  nerdctl run --rm -ti $argv
end

alias d docker
alias dco docker-compose
alias n nerdctl
