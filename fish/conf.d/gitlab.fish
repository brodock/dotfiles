set gitlab_token_file "$HOME/.gitlab-token"

if test -e $gitlab_token_file
  set GITLAB_TOKEN (cat $gitlab_token_file)
  export GITLAB_TOKEN
end
