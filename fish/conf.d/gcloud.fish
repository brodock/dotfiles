set -g -x "CLOUDSDK_PYTHON" "/opt/homebrew/bin/python3"
set gcs_include "/opt/homebrew/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.fish.inc"

if test -e $gcs_include
  source $gcs_include
  alias gc gcloud
end

