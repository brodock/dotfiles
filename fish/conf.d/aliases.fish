# Ping Mullvad API to tell my IP and associated data
function whoami-mullvad -d "Ping Mullvad API to tell my IP and associated data"
  curl -s https://ipv4.am.i.mullvad.net/json | jq
  curl -s https://ipv6.am.i.mullvad.net/json | jq
end
