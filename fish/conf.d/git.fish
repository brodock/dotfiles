function git-main -d "List git main branch" -a origin
  if test -d $origin
    set origin "origin"
  end

  git ls-remote --symref $origin HEAD | grep 'ref:' | tr '\t' ' ' | cut -d' ' -f2 | cut -d '/' -f3
end

alias g git
